<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function store(Request $req)
    {
       
       $task=new Task;
       $this->validate($req,[
        'task1'=>'required|max:100|min:3',
        ]);
       $task->task=$req->task1;
       $task->save();
       $data=Task::all();
       return view('task')->with(task,$data);
    }
    public function updatetask($id)
    {
        $task=Task::find($id);
        $task->iscompleted=1;
        $task->save();
        return redirect('/task');

    }
    public function updatetasknot($id)
    {
        $task=Task::find($id);
        $task->iscompleted=0;
        $task->save();
        return redirect('/task');

    }
    public function deletetask($id)
    {
        $task=Task::find($id);
        $task->delete();
        
        return redirect()->back();

    }

    public function updatetaskview($id)
    {
        $task=Task::find($id);
       
        
        return view('updatetask')->with('taskdata',$task); 

    }
    public function savetask(Request $req)
    {
       
       $id=$req->id;
       $task=$req->task1;
       $data=Task::find($id);
       $data->task=$task;
       $data->save();
       $datas=Task::all();
       return view('task')->with('task',$datas);
       
    }
}
