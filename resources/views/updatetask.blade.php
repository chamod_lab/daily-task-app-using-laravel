<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Update Task</title>
</head>
<body>
<div class="container" style="margin-bottom:50px;">
        <h1 style="text-align:center;">Update Daily Tasks</h1>
        
        
        <form method="post" action="/updatedata">
        {{csrf_field()}}
        <div class="form-group">
        <label for="usr">Task:</label>
        <input type="text" value="{{$taskdata->task}}" class="form-control" name="task1">
        <input type="hidden" value="{{$taskdata->id}}" class="form-control" name="id">
        </div> 
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-warning">Cancel</button>   
        </form>
    </div>
</body>
</html>