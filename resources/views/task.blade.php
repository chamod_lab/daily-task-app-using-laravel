<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Task</title>
</head>
<body>
    <div class="container" style="margin-bottom:50px;">
        <h1 style="text-align:center;">Daily Tasks</h1>
        
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert" style="text-align:center;">
                {{$error}}
            </div>
        @endforeach
        <form method="post" action="/saveTask">
        {{csrf_field()}}
        <div class="form-group">
        <label for="usr">Task:</label>
        <input type="text" placeholder="Enter your task here" class="form-control" name="task1">
        </div> 
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-warning">Reset</button>   
        </form>
    </div>

    <div class="container" style="text-align:center;">
        <table class="table table-dark">
            <th>ID</th>
            <th>Task</th>
            <th>Is Completed</th>
            <th>Action</th>
            @foreach($task as $task2)
            <tr>
                <td>{{$task2->id}}</td>
                <td>{{$task2->task}}</td>
                <td>
                @if($task2->iscompleted)
                <button class="btn btn-success">Completed</button>
                @else
                <button class="btn btn-warning">Not Completed</button>
                @endif
                </td>
                <td>
                @if($task2->iscompleted)
                    <a href="/markasnotcompleted/{{$task2->id}}" class="btn btn-warning">Mark as Not Completed</a>
                    @else
                    <a href="/markascompleted/{{$task2->id}}" class="btn btn-primary">Mark as Completed</a>
                    @endif
                    <a href="/deletetask/{{$task2->id}}" class="btn btn-danger">Delete</a>
                    <a href="/updatetask/{{$task2->id}}" class="btn btn-info">Update</a>
                </td>
            </tr>
            @endforeach
        </table>  
        
    </div>
    
</body>
</html>